package com.example.expositions1.services;

import com.example.expositions1.exceptions.InternalDatabaseException;

public interface TicketService {
    public void buyExpositionByIdForUser(Long id, Object username) throws InternalDatabaseException;
}
