package com.example.expositions1.services.populators;

public interface Populator<SOURCE, TARGET> {

    void populate(SOURCE source, TARGET target);

}
