package com.example.expositions1.services;

import com.example.expositions1.dto.user.UserCredsSignIn;
import com.example.expositions1.dto.user.UserCredsSignUp;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.UsernameExistsException;
import com.example.expositions1.exceptions.UsernameNotExistsException;
import com.example.expositions1.models.Role;

public interface UserService {

    void setUpUser(UserCredsSignUp userCreds) throws UsernameExistsException, InternalDatabaseException;

    Role getUserAuthoritiesByCredentials(UserCredsSignIn userCreds) throws UsernameNotExistsException, InternalDatabaseException;
}
