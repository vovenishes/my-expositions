package com.example.expositions1.services.impl;

import com.example.expositions1.database.dao.JDBCDaoFactory;
import com.example.expositions1.database.dao.JDBCUserDao;
import com.example.expositions1.dto.user.UserCredsSignIn;
import com.example.expositions1.dto.user.UserCredsSignUp;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.UsernameExistsException;
import com.example.expositions1.exceptions.UsernameNotExistsException;
import com.example.expositions1.models.Role;
import com.example.expositions1.models.UserModel;
import com.example.expositions1.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private JDBCDaoFactory daoFactory = JDBCDaoFactory.getInstance();


    @Override
    public void setUpUser(UserCredsSignUp userCreds) throws UsernameExistsException, InternalDatabaseException {
        JDBCUserDao userDao = daoFactory.createUserDao();
        Optional<UserModel> userOpt = userDao.findByEmail(userCreds.getUsername());
        if (userOpt.isPresent()) {
            throw new UsernameExistsException();
        }

        UserModel newUser = new UserModel();
        newUser.setUsername(userCreds.getUsername());
        newUser.setPassword(userCreds.getPassword());
        newUser.setRole(Role.valueOf(userCreds.getRole()));

        userDao.saveUser(newUser);

        userDao.close();
    }

    @Override
    public Role getUserAuthoritiesByCredentials(UserCredsSignIn userCreds) throws UsernameNotExistsException, InternalDatabaseException {
        JDBCUserDao userDao = daoFactory.createUserDao();

        Optional<UserModel> optUser = userDao.findByEmailAndPassword(userCreds.getUsername(), userCreds.getPassword());
        if (optUser.isEmpty()) {
            userDao.close();
            throw new UsernameNotExistsException();
        }
        userDao.close();
        return optUser.get().getRole();

    }
}
