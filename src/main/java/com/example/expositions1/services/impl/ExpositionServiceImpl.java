package com.example.expositions1.services.impl;

import com.example.expositions1.controllers.converters.Converter;
import com.example.expositions1.controllers.converters.NewExpositionDataToModelConverter;
import com.example.expositions1.database.dao.JDBCDaoFactory;
import com.example.expositions1.database.dao.JDBCExpositionDao;
import com.example.expositions1.database.dao.JDBCHallDao;
import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.dto.exposition.ExpositionData;
import com.example.expositions1.dto.exposition.ExpositionsPageData;
import com.example.expositions1.dto.page.Page;
import com.example.expositions1.dto.page.SearchParams;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ModelNotFoundException;
import com.example.expositions1.models.ExpositionModel;
import com.example.expositions1.models.ExpositionStatus;
import com.example.expositions1.models.HallModel;
import com.example.expositions1.services.ExpositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.expositions1.models.ExpositionStatus.CANCELLED;

public class ExpositionServiceImpl implements ExpositionService {

    private static final Logger LOG = LoggerFactory.getLogger(ExpositionServiceImpl.class);

    private JDBCDaoFactory daoFactory;
    private Converter<AddExpositionData, ExpositionModel> converter;

    public ExpositionServiceImpl() {
        this.daoFactory = JDBCDaoFactory.getInstance();
        converter = new NewExpositionDataToModelConverter();

    }

    @Override
    public ExpositionsPageData getPageableExpositions(SearchParams params) throws InternalDatabaseException {
        JDBCExpositionDao expositionDao = daoFactory.createExpositionDao();
        Page<ExpositionData> expositions = expositionDao.findPageableExpositionsBySearchParams(params);

        expositionDao.close();
        return new ExpositionsPageData(
                expositions, params.getFilterField(), params.getDirection(), params.getSearchQuery()
        );
    }

    @Override
    public List<String> getAvailableHalls(AddExpositionData expositionData) throws InternalDatabaseException {
        JDBCHallDao hallsDao = daoFactory.createHallDao();

        List<HallModel> hallModels = hallsDao.findForExpositionDate(Date.valueOf(expositionData.getDate()));

        hallsDao.close();

        return hallModels.stream()
                .map(hall -> hall.getNumber().toString())
                .collect(Collectors.toList());
    }

    @Override
    public void cancelExpositionById(Long id) throws InternalDatabaseException, ModelNotFoundException {
        JDBCExpositionDao expositionDao = daoFactory.createExpositionDao();
        ExpositionModel expositionModel = expositionDao.findLazyById(id).orElseThrow(ModelNotFoundException::new);

        expositionModel.setStatus(CANCELLED);

        expositionDao.update(expositionModel);
        expositionDao.close();
    }

    @Override
    public void buyExpositionByIdForUser(Long expositionId, Object username) {

    }


}
