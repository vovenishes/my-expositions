package com.example.expositions1.services;

import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.AvailableHallsNotFound;
import com.example.expositions1.exceptions.InternalDatabaseException;

public interface SessionService {

    void registerNewExpositionsInAvailableHalls(AddExpositionData expositionData) throws InternalDatabaseException, AvailableHallsNotFound;


}
