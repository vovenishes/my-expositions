package com.example.expositions1.services;

import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.dto.exposition.ExpositionsPageData;
import com.example.expositions1.dto.page.SearchParams;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ModelNotFoundException;

import java.util.List;

public interface ExpositionService {

    ExpositionsPageData getPageableExpositions(SearchParams params) throws InternalDatabaseException;

    List<String> getAvailableHalls(AddExpositionData expositionData) throws InternalDatabaseException;

    void cancelExpositionById(Long id) throws InternalDatabaseException, ModelNotFoundException;

    void buyExpositionByIdForUser(Long expositionId, Object username);
}
