package com.example.expositions1.models;

public enum ExpositionStatus {

    ACTIVE, CANCELLED;

}
