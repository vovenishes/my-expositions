package com.example.expositions1.models;

public class SessionModel {

    private int id;
    private ExpositionModel expositionModel;
    private HallModel hallModel;


    public SessionModel(ExpositionModel expositionModel, HallModel hallModel) {
        this.expositionModel = expositionModel;
        this.hallModel = hallModel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ExpositionModel getExpositionModel() {
        return expositionModel;
    }

    public void setExpositionModel(ExpositionModel expositionModel) {
        this.expositionModel = expositionModel;
    }

    public HallModel getHallModel() {
        return hallModel;
    }

    public void setHallModel(HallModel hallModel) {
        this.hallModel = hallModel;
    }
}
