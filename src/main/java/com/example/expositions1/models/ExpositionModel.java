package com.example.expositions1.models;

import java.sql.Date;

public class ExpositionModel {

    private Long id;
    private String title;
    private Integer cost;
    private Date date;
    private ExpositionStatus status;
    private HallModel hallModel;


    public ExpositionModel() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ExpositionStatus getStatus() {
        return status;
    }

    public void setStatus(ExpositionStatus status) {
        this.status = status;
    }

    public HallModel getHallModel() {
        return hallModel;
    }

    public void setHallModel(HallModel hallModel) {
        this.hallModel = hallModel;
    }
}
