package com.example.expositions1.exceptions;

import java.sql.SQLException;

public class InternalDatabaseException extends Exception{
    public InternalDatabaseException() {
    }

    public InternalDatabaseException(Exception e) {
        super(e);

    }
    //do not need realisations
}
