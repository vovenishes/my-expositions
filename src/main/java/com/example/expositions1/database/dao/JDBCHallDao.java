package com.example.expositions1.database.dao;

import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.HallModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class JDBCHallDao extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(JDBCUserDao.class);

    public JDBCHallDao(Connection connection) {
        super(connection);
    }


    public List<HallModel> findForExpositionDate(Date date) throws InternalDatabaseException {

        String query = "select * from halls where id not in (select hall_id from expositions join sessions s on expositions.id = s.exposition_id where date = ?  and expositions.status = 'ACTIVE')";
        List<HallModel> halls = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setDate(1, date);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    halls.add(getHallFromResultSet(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO: log
            throw new InternalDatabaseException();
        }
        return halls;
    }

    public List<HallModel> findByNumbers(List<Integer> hallNumbers) throws InternalDatabaseException {

        StringBuilder queryBuilder = new StringBuilder("select * from halls where number in (")
                .append(hallNumbers.stream().map(number -> "?").collect(joining(", ")))
                .append(")");
        List<HallModel> halls = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(queryBuilder.toString())) {
            int positionInQuery = 1;
            for (Integer number : hallNumbers) {
                statement.setInt(positionInQuery++, number);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    halls.add(getHallFromResultSet(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO: log
            throw new InternalDatabaseException();
        }
        return halls;
    }


    private static HallModel getHallFromResultSet(ResultSet res) throws SQLException {
        HallModel hall = new HallModel();
        hall.setId(res.getLong("id"));
        hall.setNumber(res.getInt("number"));
        hall.setSeats(res.getInt("seats"));
        return hall;
    }
}
