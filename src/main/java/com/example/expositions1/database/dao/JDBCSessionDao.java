package com.example.expositions1.database.dao;

import com.example.expositions1.models.SessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class JDBCSessionDao extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(JDBCSessionDao.class);

    public JDBCSessionDao(Connection connection) {
        super(connection);
    }


    public void saveAll(List<SessionModel> sessions) throws SQLException {

        connection.setAutoCommit(false);
        try (PreparedStatement statementCreateExposition = connection.prepareStatement("insert into expositions (title, cost, date, status) values (?, ?, ?, ?) returning id");
             PreparedStatement statementCreateSession = connection.prepareStatement("insert into sessions (exposition_id, hall_id) values (?, ?)", Statement.RETURN_GENERATED_KEYS)) {

            statementCreateExposition.setString(1, sessions.get(0).getExpositionModel().getTitle());
            statementCreateExposition.setInt(2, sessions.get(0).getExpositionModel().getCost());
            statementCreateExposition.setDate(3, sessions.get(0).getExpositionModel().getDate());
            statementCreateExposition.setString(4, sessions.get(0).getExpositionModel().getStatus().toString());

            long exposition_id;

            try (ResultSet resultSet = statementCreateExposition.executeQuery()) {
                if (resultSet.next()) {
                    exposition_id = resultSet.getLong("id");
                } else {
                    throw new SQLException();
                }
            }
            for (SessionModel session : sessions) {
                statementCreateSession.setLong(1, exposition_id);
                statementCreateSession.setLong(2, session.getHallModel().getId());
                statementCreateSession.addBatch();
                statementCreateSession.executeBatch();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            connection.rollback();
            connection.setAutoCommit(true);
            throw ex;
        }
        connection.commit();
        connection.setAutoCommit(true);
    }


}
