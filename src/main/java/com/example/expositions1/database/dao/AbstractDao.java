package com.example.expositions1.database.dao;

import java.sql.Connection;

public abstract class AbstractDao implements AutoCloseable
{

    protected Connection connection;

    public AbstractDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
