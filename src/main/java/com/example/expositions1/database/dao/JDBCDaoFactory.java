package com.example.expositions1.database.dao;

import com.example.expositions1.database.connection.ConnectionPool;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.HallModel;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.isNull;

public class JDBCDaoFactory {
    private DataSource dataSource;
    private static JDBCDaoFactory jdbcDaoFactory;

    private JDBCDaoFactory() {
        this.dataSource = ConnectionPool.getDataSource();

    }

    public JDBCUserDao createUserDao() throws InternalDatabaseException {
        return new JDBCUserDao(getConnection());
    }

    public JDBCExpositionDao createExpositionDao() throws InternalDatabaseException {
        return new JDBCExpositionDao(getConnection());
    }

    public JDBCSessionDao createSessionDao() throws InternalDatabaseException {
        return new JDBCSessionDao(getConnection());
    }

    public JDBCHallDao createHallDao() throws InternalDatabaseException {
        return new JDBCHallDao(getConnection());
    }

    private Connection getConnection() throws InternalDatabaseException {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new InternalDatabaseException(e);
        }
    }

    public static synchronized JDBCDaoFactory getInstance() {
        if (isNull(jdbcDaoFactory)){
            jdbcDaoFactory=new JDBCDaoFactory();
        }
        return jdbcDaoFactory;
    }
}

