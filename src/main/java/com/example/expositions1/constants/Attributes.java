package com.example.expositions1.constants;

public interface Attributes {

    String ERROR_ADD_EXPOSITION = "errorAddExposition";
    String ERROR_EXPOSITION_FIELDS_NOT_VALID = "errorAddExpositionNotValid";


    String ERROR_USER_LOGIN = "errorUserLogin";
    String ERROR_USER_REGISTER = "errorUserRegister";

    String INTERNAL_SERVER_ERROR = "internalServerError";




    String WARN_HALLS_NOT_FOUND = "warnHallsNotFound";


}
