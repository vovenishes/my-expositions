package com.example.expositions1.constants;

import com.example.expositions1.dto.page.PageFilters;

import static com.example.expositions1.dto.page.Direction.DESC;

public interface Parameters {
    interface Keys {
        String SORT_FIELD = "f";
        String DIRECTION = "d";
        String SEARCH_QUERY = "q";
        String PAGE = "p";
        String PAGE_SIZE = "s";

        String TITLE = "title";
        String DATE = "date";
        String COST = "cost";
        String HALLS = "halls";
    }

    interface Defaults {
        String PAGE_DEFAULT = "1";
        String PAGE_SIZE_DEFAULT = "5";
        String SEARCH_QUERY_DEFAULT = "";
        String DIRECTION_DEFAULT = DESC.getDirection();
        String SORT_FIELD_DEFAULT = PageFilters.TITLE.getSortField();
    }
}
