package com.example.expositions1.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatTag {

    private DateFormatTag() {}

    public static String format(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

}
