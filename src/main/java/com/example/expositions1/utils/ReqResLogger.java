package com.example.expositions1.utils;

import org.slf4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReqResLogger {

    private static final String CONTEXT = "/expositions";

    public static void redirectWithLogging(Logger logger, HttpServletResponse response, String uri) throws IOException {
        logger.info("[R]: {}", uri);
        response.sendRedirect(CONTEXT + uri);
    }

    public static void forwardWithLogging(Logger logger, HttpServletRequest request,  HttpServletResponse response, String page)
            throws IOException, ServletException {
        logger.info("[F]: {}", page);
        request.getRequestDispatcher(page).forward(request, response);
    }




}
