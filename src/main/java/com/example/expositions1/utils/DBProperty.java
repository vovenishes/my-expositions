package com.example.expositions1.utils;

import com.example.expositions1.exceptions.InternalDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBProperty {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBProperty.class.getName());

    private static final String FILE_PATH = "statements.properties";

    private static DBProperty instance;

    private Properties properties;


    private DBProperty() throws InternalDatabaseException {

        properties = new Properties();

        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PATH)){
            properties.load(inputStream);

        } catch (IOException ex) {
            LOGGER.error("[{}]{}:{};[{}]{}", "CATCH", ex.getClass().getSimpleName(),ex.getMessage(), "THROW NEW", InternalDatabaseException.class.getSimpleName());
            throw new InternalDatabaseException();
        }
    }

    public static synchronized DBProperty getInstance() throws InternalDatabaseException {
        if(instance == null) {
            return new DBProperty();
        }
        return instance;
    }

    public  String get(String key) {
        return properties.getProperty(key);
    }
}
