package com.example.expositions1.dto.exposition;

import java.util.List;
import java.util.Map;

public class AddExpositionData {

    private String title;
    private String date;
    private String cost;
    private List<String> halls;

    public AddExpositionData(String title, String date, String cost, List<String> halls) {
        this.title = title;
        this.date = date;
        this.cost = cost;
        this.halls = halls;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public List<String> getHalls() {
        return halls;
    }

    public void setHalls(List<String> halls) {
        this.halls = halls;
    }
}
