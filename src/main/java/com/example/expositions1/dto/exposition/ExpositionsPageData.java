package com.example.expositions1.dto.exposition;

import com.example.expositions1.dto.page.Page;

public class ExpositionsPageData {

    private Page<ExpositionData> expositions;

    private String filterField;

    private String direction;

    private String searchQuery;

    public ExpositionsPageData(Page<ExpositionData> expositions, String filterField, String direction, String searchQuery) {
        this.expositions = expositions;
        this.filterField = filterField;
        this.direction = direction;
        this.searchQuery = searchQuery;
    }

    public Page<ExpositionData> getExpositions() {
        return expositions;
    }

    public void setExpositions(Page<ExpositionData> expositions) {
        this.expositions = expositions;
    }

    public String getFilterField() {
        return filterField;
    }

    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
}
