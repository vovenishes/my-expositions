package com.example.expositions1.dto.page;

public enum Direction {
    ASC("asc"),
    DESC("desc");

    private String direction;

    Direction(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
