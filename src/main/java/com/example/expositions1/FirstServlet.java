package com.example.expositions1;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.command.expositions.ExpositionsAddCommand;
import com.example.expositions1.controllers.command.expositions.ExpositionsCancelCommand;
import com.example.expositions1.controllers.command.expositions.ExpositionsListCommand;
import com.example.expositions1.controllers.command.ticket.TicketBuyCommand;
import com.example.expositions1.controllers.command.user.LogOutCommand;
import com.example.expositions1.controllers.command.user.LoginCommand;
import com.example.expositions1.controllers.command.user.RegistrationCommand;
import com.example.expositions1.utils.ReqResLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static com.example.expositions1.constants.Endpoints.Regex.*;

public class FirstServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(FirstServlet.class);

    private final Map<String, Command> commands = new HashMap<>();


    @Override
    public void init(ServletConfig servletConfig) {

        servletConfig.getServletContext().setAttribute("loggedUsers", new HashSet<String>());
//auth
        commands.put(LOGIN_R, new LoginCommand());
        commands.put(REGISTRATION_R, new RegistrationCommand());
        commands.put(LOGOUT_R, new LogOutCommand());

//expositions
        commands.put(EXPOSITION_LIST_R, new ExpositionsListCommand());
        commands.put(ADD_EXPOSITION_R, new ExpositionsAddCommand());
        commands.put(CANCEL_EXPOSITION_BY_ID_R, new ExpositionsCancelCommand());
        commands.put(BUY_EXPOSITION_R, new TicketBuyCommand());
    }

    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        LOG.info("{}, {}, {}", req.getContextPath(), req.getSession().getServletContext().getContextPath(), req.getServletPath());

        String uri = req.getRequestURI().replaceAll("^/expositions", "");

        LOG.info("[{}] [{}]", req.getMethod(), req.getRequestURI());

        Command command = getCommandByURI(uri);

        LOG.info("Handler - [{}]", command.getClass().getName());

        String page = command.execute(req, res);

        if (page.contains("redirect:")) {
            ReqResLogger.redirectWithLogging(LOG, res, page.replace("redirect:", ""));
        } else {
            ReqResLogger.forwardWithLogging(LOG, req, res, page);
        }


    }

    private Command getCommandByURI(String uri) {
        Optional<String> regex = commands.keySet().stream().filter(uri::matches).findFirst();
        return commands.get(regex.isPresent() ? regex.get() : "\\/?");
    }

}
