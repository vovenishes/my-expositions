package com.example.expositions1.controllers.converters;

import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.models.ExpositionModel;

import java.sql.Date;

import static com.example.expositions1.models.ExpositionStatus.ACTIVE;

public class NewExpositionDataToModelConverter implements Converter<AddExpositionData, ExpositionModel> {
    @Override
    public ExpositionModel convert(AddExpositionData request) {
        ExpositionModel expositionModel = new ExpositionModel();

        expositionModel.setTitle(request.getTitle());
        expositionModel.setCost(Integer.parseInt(request.getCost()));
        expositionModel.setDate(Date.valueOf(request.getDate()));
        expositionModel.setStatus(ACTIVE);
        return expositionModel;

    }
}
