package com.example.expositions1.controllers.converters;

import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.ValidationException;

import javax.servlet.http.HttpServletRequest;

import static com.example.expositions1.constants.Parameters.Keys.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

public class AddExpositionConverter implements Converter<HttpServletRequest, AddExpositionData> {

    @Override
    public AddExpositionData convert(HttpServletRequest request) {

        String[] halls = request.getParameterValues(HALLS);

        return new AddExpositionData(
                request.getParameter(TITLE),
                request.getParameter(DATE),
                request.getParameter(COST),
                asList(isNull(halls) ? new String[0] : halls)
        );

    }

}
