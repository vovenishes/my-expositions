package com.example.expositions1.controllers.filters;


import com.example.expositions1.models.Role;
import com.example.expositions1.utils.ReqResLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.isNull;

public class AuthFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(AuthFilter.class);
    private static final String ROLE_ATTRIBUTE = "role";
    private static final String BASE_PAGE_URI = "/overview";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)res;

        String uri = request.getRequestURI().replace("/expositions", "");

        populateResponseHeaders(response);

        Role role = retrieveRoleFromSession(request.getSession());
        LOG.info("User: {}", role.name());
        if(hasAuthorityToAccess(role, uri)) {
            LOG.info("{} has authority to access {}", role.name(), uri);
            chain.doFilter(req, res);
        }else {
            LOG.warn("{} hasn't authority to access {}", role.name(), uri);
            ReqResLogger.redirectWithLogging(LOG, response, BASE_PAGE_URI);
        }
    }

    private boolean hasAuthorityToAccess(Role role, String uri) {
        return role.getEndpointPatterns().stream().anyMatch(uri::matches);
    }

    private void populateResponseHeaders(HttpServletResponse response) {
        response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    }

    private Role retrieveRoleFromSession(HttpSession session) {
        String roleName = (String)session.getAttribute(ROLE_ATTRIBUTE);
        if(isNull(roleName)) {
            roleName = Role.GUEST.name();
            session.setAttribute(ROLE_ATTRIBUTE, roleName);
        }
        return Role.valueOf(roleName);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
