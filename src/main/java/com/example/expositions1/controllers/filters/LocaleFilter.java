package com.example.expositions1.controllers.filters;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static java.util.Objects.isNull;

public class LocaleFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(LocaleFilter.class);

    private static final String LOCALE = "locale";
    private static final String LANG = "lang";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        String setLang = req.getParameter(LANG);

        if (setLang == null && req.getSession().getAttribute(LOCALE) == null) {
            req.getSession().setAttribute(LOCALE, "en");
        } else if ("en".equalsIgnoreCase(setLang) || "ru".equalsIgnoreCase(setLang)) {
            req.getSession().setAttribute(LOCALE, setLang);
        }
        LOG.info("Locale: [current lang: {}, set lang to: {}]", req.getSession().getAttribute(LOCALE), isNull(req.getParameter(LANG)) ? "none" : req.getParameter(LANG));
        chain.doFilter(request, response);
    }


}
