package com.example.expositions1.controllers.command;

import com.example.expositions1.models.Role;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;

public class CommandUtility {

    private CommandUtility() {
    }

    public static void addUserToSession(HttpServletRequest req, Role role, String username) {
        req.getSession().setAttribute("username", username);
        req.getSession().setAttribute("role", role.name());
    }

    public static boolean checkUserIsLogged(HttpServletRequest req, String username) {
        @SuppressWarnings("unchecked")
        HashSet<String> loggedUsers = (HashSet<String>) req.getSession().getServletContext().getAttribute("loggedUsers");
        if (loggedUsers.stream().anyMatch(username::equals)) {
            return true;
        }
        return false;
    }


    public static void markUserAsLogged(HttpServletRequest req, String username) {
        @SuppressWarnings("unchecked")
        HashSet<String> loggedUsers = (HashSet<String>) req.getSession().getServletContext().getAttribute("loggedUsers");
        loggedUsers.add(username);
        req.getSession().getServletContext().setAttribute("loggedUsers", loggedUsers);
    }

    public static void deleteUserFromContext(HttpServletRequest req) {
        ServletContext context = req.getServletContext();
        @SuppressWarnings("unchecked")
        HashSet<String> loggedUsers = (HashSet<String>) context.getAttribute("loggedUsers");

        loggedUsers.remove((String) req.getSession().getAttribute("username"));

        context.setAttribute("loggedUsers", loggedUsers);
    }

    public static String getLocale(HttpServletRequest req) {
        return (String) req.getSession().getAttribute("locale");
    }
}
