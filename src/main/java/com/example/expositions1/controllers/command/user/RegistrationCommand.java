package com.example.expositions1.controllers.command.user;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.dto.user.UserCredsSignUp;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.UsernameExistsException;
import com.example.expositions1.services.UserService;
import com.example.expositions1.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.example.expositions1.constants.Attributes.*;
import static java.util.Objects.isNull;


public class RegistrationCommand implements Command {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationCommand.class);

    private UserService userService;

    public RegistrationCommand() {
        this.userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserCredsSignUp userCreds = new UserCredsSignUp(
                request.getParameter("username"),
                request.getParameter("password"),
                request.getParameter("role"));

        LOG.info("User creds to sign in: {}", userCreds);

        if (isNull(userCreds.getUsername()) || isNull(userCreds.getPassword()) || isNull(userCreds.getRole())) {
            LOG.error("User creds are not valid");
            request.setAttribute("user", userCreds);
            if (!"GET".equals(request.getMethod())) {
                request.setAttribute(ERROR_USER_REGISTER, "please.set.input.fields.correctly");
            }
            // TODO: Add error message attribute
            return "/WEB-INF/user/registration.jsp";
        }
        try {
            userService.setUpUser(userCreds);
            LOG.info("User {} was registered as {}", userCreds.getUsername(), userCreds.getRole());
        } catch (UsernameExistsException e) {
            LOG.error("User {} already exists in database", userCreds.getUsername());
            request.setAttribute(ERROR_USER_REGISTER, "user.with.such.username.already.exists");
            // TODO: Add error message attribute
            return "/WEB-INF/user/registration.jsp";
        } catch (InternalDatabaseException e) {
            LOG.error("User {} wasn't registered", userCreds.getUsername());
            request.setAttribute(INTERNAL_SERVER_ERROR, "internal.error.on.server.500");
            return "/WEB-INF/errors/internalServerError.jsp";
        }
        return "/WEB-INF/user/login.jsp";
    }
}
