package com.example.expositions1.controllers.command.expositions;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.converters.Converter;
import com.example.expositions1.controllers.converters.ProductConverter;
import com.example.expositions1.dto.page.SearchParams;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ValidationException;
import com.example.expositions1.services.ExpositionService;
import com.example.expositions1.services.impl.ExpositionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExpositionsListCommand implements Command {

    private ExpositionService expositionService;
    private Converter<HttpServletRequest, SearchParams> converter;

    public ExpositionsListCommand() {
        this.expositionService = new ExpositionServiceImpl();
        converter = new ProductConverter();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        SearchParams searchParams = null;
        searchParams = converter.convert(request);

        try {
            request.setAttribute("expositions", expositionService.getPageableExpositions(searchParams));
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
            // TODO: Add error message attribute
            e.printStackTrace();
        }


        return "/WEB-INF/expositions/expositions.jsp";
    }
}
