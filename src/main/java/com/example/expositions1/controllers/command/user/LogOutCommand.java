package com.example.expositions1.controllers.command.user;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.command.CommandUtility;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogOutCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        CommandUtility.deleteUserFromContext(request);
        CommandUtility.addUserToSession(request, Role.GUEST, null);

        return "redirect:/overview";
    }
}
