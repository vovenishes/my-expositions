package com.example.expositions1.controllers.command.user;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.command.CommandUtility;
import com.example.expositions1.dto.user.UserCredsSignIn;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.UsernameNotExistsException;
import com.example.expositions1.models.Role;
import com.example.expositions1.services.UserService;
import com.example.expositions1.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.example.expositions1.constants.Attributes.*;
import static java.util.Objects.isNull;

public class LoginCommand implements Command {

    private static final Logger LOG = LoggerFactory.getLogger(LoginCommand.class);

    private UserService userService;

    public LoginCommand() {
        userService = new UserServiceImpl();
    }


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        UserCredsSignIn userCreds = new UserCredsSignIn(
                request.getParameter("username"),
                request.getParameter("password"));


        LOG.info("User creds to sign in: {}", userCreds);
        if (isNull(userCreds.getUsername()) || isNull(userCreds.getPassword())) {
            LOG.error("User creds are not valid");
            request.setAttribute("user", userCreds);
            // TODO: Add error message attribute
            if (!"GET".equals(request.getMethod())) {
                request.setAttribute(ERROR_USER_LOGIN, "please.set.input.fields.correctly");
            }
            return "/WEB-INF/user/login.jsp";
        }


        if (CommandUtility.checkUserIsLogged(request, userCreds.getUsername())) {
            LOG.error("User {} has already logged in system", userCreds.getUsername());
            request.setAttribute(ERROR_USER_LOGIN, "user.already.logged.in.system");
            return "/WEB-INF/user/login.jsp";
        }
        Role role;
        try {
            role = userService.getUserAuthoritiesByCredentials(userCreds);
            CommandUtility.addUserToSession(request, role, userCreds.getUsername());
            CommandUtility.markUserAsLogged(request, userCreds.getUsername());
            LOG.info("User {} was authorized as {}", userCreds.getUsername(), role);
        } catch (UsernameNotExistsException ex) {
            LOG.error("User {} not found", userCreds.getUsername());
            CommandUtility.addUserToSession(request, Role.GUEST, null);
            // TODO: Add error message attribute
                request.setAttribute(ERROR_USER_LOGIN, "user.not.found.with.such.credentials");
            return "/WEB-INF/user/login.jsp";
        } catch (InternalDatabaseException e) {
            LOG.error("User {} wasn't authorized due to internal errors", userCreds.getUsername());
            // TODO: Add error message attribute
            return "/WEB-INF/user/registration.jsp";
        }
        return "redirect:/overview";
    }
}
