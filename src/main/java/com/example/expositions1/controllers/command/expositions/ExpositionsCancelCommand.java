package com.example.expositions1.controllers.command.expositions;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.converters.AddExpositionConverter;
import com.example.expositions1.controllers.converters.Converter;
import com.example.expositions1.controllers.validators.AddExpositionBasicFieldsValidator;
import com.example.expositions1.controllers.validators.Validator;
import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ModelNotFoundException;
import com.example.expositions1.exceptions.ValidationException;
import com.example.expositions1.services.ExpositionService;
import com.example.expositions1.services.SessionService;
import com.example.expositions1.services.impl.ExpositionServiceImpl;
import com.example.expositions1.services.impl.SessionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpositionsCancelCommand implements Command {

    private static final Pattern ID_REGEX = Pattern.compile("(?<=\\/cancel\\/)[0-9]+(?=\\/?$)");

    private ExpositionService expositionService;

    public ExpositionsCancelCommand() {
        this.expositionService = new ExpositionServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Long id = null;
        try {
            id = Long.valueOf(getExpositionId(request));
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        try {
            expositionService.cancelExpositionById(id);
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
        } catch (ModelNotFoundException e) {
            e.printStackTrace();
        }
        return "redirect:/overview";
    }

    private static String getExpositionId(HttpServletRequest request) throws ValidationException {
        Matcher matcher = ID_REGEX.matcher(request.getRequestURI());
        if(matcher.find()) {
            return matcher.group();
        }
        throw new ValidationException("Not valid");
    }

}
