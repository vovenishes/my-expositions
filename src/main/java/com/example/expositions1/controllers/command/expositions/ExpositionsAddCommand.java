package com.example.expositions1.controllers.command.expositions;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.controllers.converters.AddExpositionConverter;
import com.example.expositions1.controllers.converters.Converter;
import com.example.expositions1.controllers.validators.AddExpositionBasicFieldsValidator;
import com.example.expositions1.controllers.validators.Validator;
import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.AvailableHallsNotFound;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ValidationException;
import com.example.expositions1.services.ExpositionService;
import com.example.expositions1.services.SessionService;
import com.example.expositions1.services.impl.ExpositionServiceImpl;
import com.example.expositions1.services.impl.SessionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.example.expositions1.constants.Attributes.*;

public class ExpositionsAddCommand implements Command {

    private ExpositionService expositionService;
    private SessionService sessionService;
    private Converter<HttpServletRequest, AddExpositionData> converter;
    private Validator<AddExpositionData> validator;

    public ExpositionsAddCommand() {
        this.expositionService = new ExpositionServiceImpl();
        this.sessionService = new SessionServiceImpl();
        converter = new AddExpositionConverter();
        validator = new AddExpositionBasicFieldsValidator();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        AddExpositionData expositionData = converter.convert(request);
        try {
            validator.validate(expositionData);
        } catch (ValidationException e) {
            e.printStackTrace();
            if (!"GET".equals(request.getMethod())) {
                request.setAttribute(ERROR_EXPOSITION_FIELDS_NOT_VALID, "please.set.input.fields.correctly");
            }
            request.setAttribute("exposition", expositionData);
            return "/WEB-INF/expositions/addExposition.jsp";
        }

        try {
            if (expositionData.getHalls().isEmpty()) {
                expositionData.setHalls(expositionService.getAvailableHalls(expositionData));
                if (expositionData.getHalls().isEmpty()) {
                        request.setAttribute(WARN_HALLS_NOT_FOUND, "available.halls.not.found.for.this.date");
                       expositionData.setHalls(null);
                }
                request.setAttribute("exposition", expositionData);
                return "/WEB-INF/expositions/addExposition.jsp";
            } else {
                sessionService.registerNewExpositionsInAvailableHalls(expositionData);
                return "redirect:/overview";
            }
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
            // TODO: Add error message attribute
        } catch (AvailableHallsNotFound e) {
            e.printStackTrace();
            request.setAttribute(WARN_HALLS_NOT_FOUND, "available.halls.not.found.for.this.date");
            expositionData.setHalls(null);
            request.setAttribute("exposition", expositionData);
            return "/WEB-INF/expositions/addExposition.jsp";
        }

        return "redirect:/overview";
    }
}
