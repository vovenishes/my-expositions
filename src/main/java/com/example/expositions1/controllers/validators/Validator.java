package com.example.expositions1.controllers.validators;

import com.example.expositions1.exceptions.ValidationException;

public interface Validator<T> {

    public void validate(T request) throws ValidationException;

}
