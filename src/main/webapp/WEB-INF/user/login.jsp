<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>



<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../fragments/head.jsp"/>
    <title><fmt:message key="title.sign"/></title>

</head>
<body>
<jsp:include page="../fragments/header.jsp"/>

<div class="topic">
    <div class="header">
        <div class="container">

        </div>
    </div>
    <section class="form">
        <div class="container">
            <div class="form__inner">
                <h2 class="form__title">Login</h2>

                <c:if test="${errorUserLogin != null}">
                    <div class="alert alert-danger">
                        <c:out value="${errorUserLogin}"></c:out>
                    </div>
                </c:if>

                <form class="form__user" method="post" action='<c:url value="/login"/>'>
                    <input class="form__input" type="text" placeholder="<fmt:message key="user.username"/>"
                           name="username" value="<c:out value="${user.username }" />">
                    <input class="form__input" type="password" placeholder="<fmt:message key="user.password"/>"
                           name="password" value="<c:out value="${user.password }" />">
                    <%--                    <input id="guest_radio" class="form__input" type="radio" value="GUEST" name=role>--%>
                    <%--                    <label for="guest_radio"><fmt:message key="role.guest"/></label>--%>
                    <button class="form__btn" type="submit">Login</button>
                </form>
            </div>
        </div>

    </section>
    <footer class="footer"></footer>
</div>
</html>

</body>
</html>
