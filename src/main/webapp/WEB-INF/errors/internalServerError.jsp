<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../fragments/head.jsp"/>
    <title><fmt:message key="title.expositions"/></title>

</head>
<body>

<jsp:include page="../fragments/header.jsp"/>

<div class="text-center">
    <div class="sectionMainContent">

        <c:if test="${internalServerError != null}">
            <div class="alert alert-danger">
                <c:out value="${internalServerError}"></c:out>
            </div>
        </c:if>

    </div>
</div>

</body>
</html>