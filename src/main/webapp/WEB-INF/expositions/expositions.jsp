<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../fragments/head.jsp"/>
    <title><fmt:message key="title.expositions"/></title>

</head>
<body>

<jsp:include page="../fragments/header.jsp"/>


<div class="sectionMainContent  text-center">
    <form action='<c:url value="/overview"/>'>
        <div class="d-flex shadow p-3 mb-5 mt-5 bg-light rounded">
            <div class="flex-grow-1">
                <div class=" input-group">
                    <input type="text" class="form-control"
                           placeholder="<fmt:message key="expositions.input.title.or.cost"/>"
                           aria-describedby="basic-addon1" name='q'
                           value="<c:out value="${expositions.searchQuery}" />">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <fmt:message key="expositions.find"/>
                        </button>
                    </div>
                </div>
            </div>
            <c:if test="${sessionScope.role == 'ADMIN'}">
                <div class="ml-2">
                    <a href='<c:url value="/add"/>'>
                        <div title="<fmt:message key="expositions.add" />"
                             class="bi bi-plus-square btn btn-primary"></div>
                    </a>
                </div>
            </c:if>
        </div>
    </form>


    <c:if test="${expositions.expositions.hasContent}">
        <c:set var="url" value='?d='/>
        <c:choose>
            <c:when test="${expositions.direction == null || expositions.direction == 'asc'}">
                <c:set var="urlSort" value="${url}desc"/>
            </c:when>
            <c:when test="${expositions.direction == 'desc'}">
                <c:set var="urlSort" value="${url}asc"/>
            </c:when>
        </c:choose>
        <div class="d-flex justify-content-center shadow p-3 bg-light rounded">
            <div class="">
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th>
                            <a href='<c:url value="${urlSort}"><c:param name="f" value="id"/></c:url>'>
                                <fmt:message key="exposition.id"/>
                            </a>
                        </th>
                        <th class="titleColumn">
                            <a href='<c:url value="${urlSort}"><c:param name="f" value="title"/></c:url>'>
                                <fmt:message key="exposition.title"/>
                            </a>
                        </th>
                        <th>
                            <a href='<c:url value="${urlSort}"><c:param name="f" value="date"/></c:url>'>
                                <fmt:message key="exposition.date"/>
                            </a>
                        </th>
                        <th>
                            <a href='<c:url value="${urlSort}"><c:param name="f" value="cost"/></c:url>'>
                                <fmt:message key="exposition.cost"/>
                            </a>
                        </th>
                        <th>
                            <a href='<c:url value="${urlSort}"><c:param name="f" value="halls"/></c:url>'>
                                <fmt:message key="exposition.halls"/>
                            </a>
                        </th>
                        <c:if test="${sessionScope.role == 'ADMIN'}">
                            <th><fmt:message key="exposition.edit"/></th>
                        </c:if>
                        <c:if test="${sessionScope.role == 'CASHIER' || sessionScope.role == 'GUEST'}">
                            <th><fmt:message key="exposition.buy"/></th>
                        </c:if>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach var="exposition" items="${expositions.expositions.items}">
                        <tr>
                            <td><c:out value="${exposition.id}"/></td>
                            <td class="titleColumn"><c:out value="${exposition.title}"/></td>
                            <td><c:out value="${exposition.date}"/></td>
                            <td><c:out value="${exposition.cost}"/></td>
                            <td><c:out value="${exposition.halls}"/></td>
                            <c:if test="${sessionScope.role == 'ADMIN'}">
                                <td><a href='<c:url value="/cancel/${exposition.id}"/>'
                                       class="btn bi bi-x-square"></a></td>
                            </c:if>
                            <c:if test="${sessionScope.role == 'USER'}">
                                <td><a href='<c:url value="/buy/${exposition.id}"/>'
                                       class="btn bi bi-bag"></a></td>
                            </c:if>
                            <c:if test="${sessionScope.role == 'GUEST'}">
                                <td><a href='<c:url value="/login"/>'
                                       class="btn bi bi-bag"></a></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="mt-5 d-flex justify-content-center">
            <nav>
                <ul class="pagination flex-wrap">
                    <c:if test="${expositions.expositions.number!=1}"><c:set var="cl" value="page-item"/></c:if>
                    <c:if test="${expositions.expositions.number==1}"><c:set var="cl" value="page-item disabled"/></c:if>


                    <c:choose>
                        <c:when test="${expositions.direction == null || expositions.direction == 'asc'}">
                            <c:set var="urlPagin" value="${url}asc"/>
                        </c:when>
                        <c:when test="${expositions.direction == 'desc'}">
                            <c:set var="urlPagin" value="${url}desc"/>
                        </c:when>
                    </c:choose>
                    <c:if test="${expositions.filterField != null && expositions.filterField != ''}">
                        <c:set var="urlPagin" value="${urlPagin}&f=${expositions.filterField}"/>
                    </c:if>
                    <c:if test="${expositions.searchQuery != null && expositions.searchQuery != ''}">
                        <c:set var="urlPagin" value="${urlPagin}&q=${expositions.searchQuery}"/>
                    </c:if>

                    <li class='<c:out value="${cl}"/>'>
                        <a href='<c:url value="${urlPagin}">
											<c:param name="p" value="1"/>
										</c:url>'
                           class="page-link"><b>&laquo;</b></a>
                    </li>

                    <li class='<c:out value="${cl}"/>'>
                        <a href='<c:url value="${urlPagin}">
											<c:param name="p" value="${expositions.expositions.number-1}"/>
										</c:url>'
                           class="page-link"><b>&lsaquo;</b></a>
                    </li>


                    <c:forEach var="i" begin="1" end="${expositions.expositions.total}">
                        <c:if test="${expositions.expositions.number!=i}"><c:set var="cm" value="page-item"/></c:if>
                        <c:if test="${expositions.expositions.number==i}"><c:set var="cm" value="page-item active"/></c:if>

                        <c:if test="${expositions.expositions.number-3>=i  && expositions.expositions.number-4<i}">
                            <li class='<c:out value="${cm}"/>'>
                                <a href='<c:url value="${urlPagin}"><c:param name="p" value="${expositions.expositions.number-3}"/></c:url>'
                                   class="page-link"><c:out value="..."/></a>
                            </li>
                        </c:if>
                        <c:if test="${expositions.expositions.number+3<=i && expositions.expositions.number+4>i}">
                            <li class='<c:out value="${cm}"/>'>
                                <a href='<c:url value="${urlPagin}"><c:param name="p" value="${expositions.expositions.number+3}"/></c:url>'
                                   class="page-link"><c:out value="..."/></a>
                            </li>
                        </c:if>
                        <c:if test="${expositions.expositions.number+3>i && expositions.expositions.number-3<i}">
                            <li class='<c:out value="${cm}"/>'>
                                <a href='<c:url value="${urlPagin}"><c:param name="p" value="${i}"/></c:url>'
                                   class="page-link"><c:out value="${i}"/></a>
                            </li>
                        </c:if>
                    </c:forEach>
                    <c:if test="${expositions.expositions.number!=expositions.expositions.total}"><c:set var="cr"
                                                                                             value="page-item"/></c:if>
                    <c:if test="${expositions.expositions.number==expositions.expositions.total}"><c:set var="cr"
                                                                                             value="page-item disabled"/></c:if>
                    <li class='<c:out value="${cr}"/>'>
                        <a href='<c:url value="${urlPagin}">
											<c:param name="p" value="${expositions.expositions.number+1}"/>
										</c:url>'
                           class="page-link"><b>&rsaquo;</b></a>
                    </li>

                    <li class='<c:out value="${cr}"/>'>
                        <a href='<c:url value="${urlPagin}">
											<c:param name="p" value="${expositions.expositions.total}"/>
										</c:url>'
                           class="page-link"><b>&raquo;</b></a>
                    </li>
                </ul>
            </nav>
        </div>
    </c:if>
    <c:if test="${!expositions.expositions.hasContent }">
        <h3 class="m-xl-5 d-flex justify-content-center">
            <fmt:message key="expositions.not.found"/>
        </h3>
    </c:if>


</div>


</body>
</html>
